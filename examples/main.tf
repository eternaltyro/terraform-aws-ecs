module "vpc" {
  source = "git::https://gitlab.com/eternaltyro/terraform-aws-vpc.git"

  project_meta = var.project_meta

  deployment_environment = var.deployment_environment
  default_tags           = var.default_tags
}

module "efs" {
  source = "git::https://gitlab.com/eternaltyro/terraform-aws-efs.git"

  mount_target_security_groups = [module.vpc.default_security_group_id]
  efs_mount_subnet             = module.vpc.private_subnets[1]
  default_tags                 = var.default_tags
}

module "ecs" {
  source = "git::https://gitlab.com/eternaltyro/terraform-aws-ecs.git"

  project_meta = var.project_meta
  aws_vpc_id   = module.vpc.vpc_id

  container_secrets = null
  container_envvars = null
  alarm_settings = {
    names    = []
    enable   = false
    rollback = false
  }

  efs_settings = {
    file_system_id          = module.efs.efs_id
    access_point_id         = null // module.efs.access_point_id
    root_directory          = "/"
    transit_encryption      = "DISABLED"
    iam_authz               = "DISABLED"
    transit_encryption_port = null // 2999
  }
  alb_settings = {
    acm_tls_cert_domain = "example.net"
    health_check_path   = "/"
    subnets             = module.vpc.public_subnets
    tls_cipher_policy   = ""
  }
  service_settings = {
    subnets             = module.vpc.private_subnets
    propagate_tags_from = "SERVICE"
  }
  container_settings = {
    app_port         = 8000
    cpu_architecture = "X86_64"
    image_url        = ""
    image_tag        = ""
    service_name     = "svcname"
  }

  log_configuration = {
    logdriver = "awslogs"
    options = {
      awslogs-group         = "test"
      awslogs-region        = "us-east-1"
      awslogs-stream-prefix = "prfx"
    }
  }

  deployment_environment = var.deployment_environment
  default_tags           = var.default_tags
}
